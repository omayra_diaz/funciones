var nombre1;

function nombre(callback) 
 {
    nombre1 = prompt('Introduce tu nombre : ');
    callback(nombre1);
} //funcion callback

function saludo (nombre1)
{
  document.write('Hola ' + nombre1 + ' que tengas un excelente dia. <br/> Esto es una funcion callback. <hr> ' )
}

nombre(saludo);


!function (){
    document.write('Esto ' + nombre1 + ' es un ejemplo de funcion anonima <hr/>')
}();// funcion anonima

(() => {

    document.write ('y esto ' + nombre1 +'  es un ejemplo de funcion flecha')
})
();//funcion flecha
